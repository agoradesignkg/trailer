<?php

namespace Drupal\trailer\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

class TrailerAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\trailer\Entity\TrailerInterface $entity */
    $result = parent::checkAccess($entity, $operation, $account);

    if ($result->isNeutral()) {
      $is_owner = $account->isAuthenticated() && $account->id() && $account->id() == $entity->getOwnerId() ? TRUE : FALSE;
      $is_published = $entity->isPublished();

      switch ($operation) {
        case 'view':
        case 'view label':
          if (!$is_published) {
            $result = AccessResult::forbidden()->addCacheableDependency($entity);
          }
          else {
            if ($is_owner) {
              $result = AccessResult::allowedIfHasPermissions($account, [
                "view own {$entity->getEntityTypeId()}",
                "view {$entity->getEntityTypeId()}",
              ], 'OR');
            }
            else {
              $result = AccessResult::allowedIfHasPermission($account, "view {$entity->getEntityTypeId()}");
            }
            $result->cachePerPermissions()->cachePerUser()->addCacheableDependency($entity);
          }
          break;

        default:
          if ($is_owner) {
            $result = AccessResult::allowedIfHasPermissions($account, [
              "$operation own {$entity->getEntityTypeId()}",
              "$operation {$entity->getEntityTypeId()}",
              "$operation own {$entity->bundle()} {$entity->getEntityTypeId()}",
              "$operation {$entity->bundle()} {$entity->getEntityTypeId()}",
            ], 'OR');
          }
          else {
            $result = AccessResult::allowedIfHasPermissions($account, [
              "$operation {$entity->getEntityTypeId()}",
              "$operation {$entity->bundle()} {$entity->getEntityTypeId()}",
            ], 'OR');
          }
          $result->cachePerPermissions()->cachePerUser()->addCacheableDependency($entity);
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $result = parent::checkCreateAccess($account, $context, $entity_bundle);
    if (empty($entity_bundle)) {
      return $result;
    }
    $operation = 'create';
    $entity_type_id = $context['entity_type_id'];
    return $result->orIf(AccessResult::allowedIfHasPermission($account, "$operation {$entity_bundle} {$entity_type_id}"));
  }

}
