<?php

namespace Drupal\trailer\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

class TrailerTypeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    $result = parent::checkAccess($entity, $operation, $account);

    if ($result->isNeutral()) {
      switch ($operation) {
        case 'view':
          $result = AccessResult::allowedIfHasPermission($account, 'access trailer overview');
          $result->cachePerPermissions()->addCacheableDependency($entity);
          break;
      }
    }

    return $result;
  }

}
