<?php

namespace Drupal\trailer\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Defines the trailer page cache context service.
 *
 * Cache context ID: 'route.trailer'.
 *
 * This allows for trailer detail page aware caching. It depends on:
 * - whether the current route represents a trailer entity at all
 * - and if so, its ID.
 */
class TrailerPageCacheContext implements CacheContextInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRoute;

  /**
   * Constructs a new TrailerPageCacheContext class.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route
   *   The route match.
   */
  public function __construct(RouteMatchInterface $current_route) {
    $this->currentRoute = $current_route;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Trailer page');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    if (!$this->currentRoute->getRouteName() == 'entity.trailer.canonical') {
      return 0;
    }
    /** @var \Drupal\trailer\entity\TrailerInterface $trailer */
    $trailer = $this->currentRoute->getParameter('trailer');
    return $trailer->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    // @todo check, if we need to add the trailer ID as cache tag.
    return new CacheableMetadata();
  }

}
