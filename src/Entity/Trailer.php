<?php

namespace Drupal\trailer\Entity;

use Drupal\commerce_price\Price;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\physical\Length;
use Drupal\physical\Weight;
use Drupal\taxonomy\TermInterface;
use Drupal\user\UserInterface;

/**
 * Defines the trailer entity class.
 *
 * @ContentEntityType(
 *   id = "trailer",
 *   label = @Translation("Trailer"),
 *   label_singular = @Translation("trailer"),
 *   label_plural = @Translation("trailers"),
 *   label_count = @PluralTranslation(
 *     singular = "@count trailer",
 *     plural = "@count trailers",
 *   ),
 *   bundle_label = @Translation("Trailer type"),
 *   handlers = {
 *     "storage" = "Drupal\trailer\TrailerStorage",
 *     "access" = "Drupal\trailer\Access\TrailerAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "view_builder" = "Drupal\trailer\ViewBuilder\TrailerViewBuilder",
 *     "list_builder" = "Drupal\trailer\TrailerListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\trailer\Form\TrailerForm",
 *       "add" = "Drupal\trailer\Form\TrailerForm",
 *       "edit" = "Drupal\trailer\Form\TrailerForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer trailer",
 *   permission_granularity = "bundle",
 *   fieldable = TRUE,
 *   translatable = TRUE,
 *   base_table = "trailer",
 *   data_table = "trailer_field_data",
 *   entity_keys = {
 *     "id" = "trailer_id",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/trailer/{trailer}",
 *     "add-page" = "/trailer/add",
 *     "add-form" = "/trailer/add/{trailer_type}",
 *     "edit-form" = "/trailer/{trailer}/edit",
 *     "delete-form" = "/trailer/{trailer}/delete",
 *     "collection" = "/admin/content/trailers"
 *   },
 *   bundle_entity_type = "trailer_type",
 *   field_ui_base_route = "entity.trailer_type.edit_form",
 * )
 */
class Trailer extends ContentEntityBase implements TrailerInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getCategory() {
    return $this->get('category')->entity;
  }

  /**
   * @inheritDoc
   */
  public function setCategory(TermInterface $category) {
    $this->set('category', $category->id());
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getCategoryId() {
    return $this->get('category')->target_id;
  }

  /**
   * @inheritDoc
   */
  public function setCategoryId($category_id) {
    $this->set('category', $category_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    return $this->set('uid', $uid);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->set('description', $description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTyres() {
    return $this->get('tyres')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTyres($tyres) {
    $this->set('tyres', $tyres);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLoadingHeight() {
    if (!$this->get('loading_height')->isEmpty()) {
      return $this->get('loading_height')->first()->toMeasurement();
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setLoadingHeight(Length $loading_height) {
    $this->set('loading_height', $loading_height);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getInternalLength() {
    if (!$this->get('internal_dimensions')->isEmpty()) {
      $value = $this->get('internal_dimensions')->length;
      $is_empty = is_null($value) || $value === '';
      return !$is_empty ? $this->get('internal_dimensions')->first()->getLength() : NULL;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getInternalWidth() {
    if (!$this->get('internal_dimensions')->isEmpty()) {
      $value = $this->get('internal_dimensions')->width;
      $is_empty = is_null($value) || $value === '';
      return !$is_empty ? $this->get('internal_dimensions')->first()->getWidth() : NULL;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getInternalHeight() {
    if (!$this->get('internal_dimensions')->isEmpty()) {
      $value = $this->get('internal_dimensions')->height;
      $is_empty = is_null($value) || $value === '';
      return !$is_empty ? $this->get('internal_dimensions')->first()->getHeight() : NULL;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLoadCapacity() {
    if (!$this->get('load_capacity')->isEmpty()) {
      return $this->get('load_capacity')->first()->toMeasurement();
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setLoadCapacity(Weight $load_capacity) {
    $this->set('load_capacity', $load_capacity);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalWeight() {
    if (!$this->get('total_weight')->isEmpty()) {
      return $this->get('total_weight')->first()->toMeasurement();
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setTotalWeight(Weight $total_weight) {
    $this->set('total_weight', $total_weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrice() {
    if (!$this->get('price')->isEmpty()) {
      return $this->get('price')->first()->toPrice();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setPrice(Price $price) {
    $this->set('price', $price);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSortWeight() {
    return $this->get('sort_weight')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSortWeight($sort_weight) {
    $this->set('sort_weight', $sort_weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The author.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDefaultValueCallback('Drupal\trailer\Entity\Trailer::getCurrentUserId');

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the trailer.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('default_value', '')
      ->setSetting('max_length', 512)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['category'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Type'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => ['trailer_categories' => 'trailer_categories'],
        'sort' => ['field' => '_none'],
        'auto_create' => FALSE,
        'auto_create_bundle' => '',
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t('A description of the trailer.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['tyres'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Tyres'))
      ->setDescription(t('The tyre dimensions.'))
      ->setSetting('default_value', '')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['loading_height'] = BaseFieldDefinition::create('physical_measurement')
      ->setLabel(t('Loading height'))
      ->setSetting('measurement_type', 'length')
      ->setDisplayOptions('form', [
        'type' => 'physical_measurement_default',
        'settings' => [
          'default_unit' => 'mm',
          'allow_unit_change' => FALSE,
        ],
        'weight' => 6,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'physical_measurement_default',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['internal_dimensions'] = BaseFieldDefinition::create('physical_dimensions')
      ->setLabel(t('Internal dimensions'))
      ->setDisplayOptions('form', [
        'type' => 'physical_dimensions_default',
        'settings' => [
          'default_unit' => 'mm',
          'allow_unit_change' => FALSE,
        ],
        'weight' => 6,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'physical_dimensions_default',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['load_capacity'] = BaseFieldDefinition::create('physical_measurement')
      ->setLabel(t('Vehicle load capacity'))
      ->setSetting('measurement_type', 'weight')
      ->setDisplayOptions('form', [
        'type' => 'physical_measurement_default',
        'settings' => [
          'default_unit' => 'kg',
          'allow_unit_change' => FALSE,
        ],
        'weight' => 8,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'physical_measurement_default',
        'weight' => 8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['total_weight'] = BaseFieldDefinition::create('physical_measurement')
      ->setLabel(t('Total weight'))
      ->setDescription(t('The Gross Vehicle Weight (GVW).'))
      ->setSetting('measurement_type', 'weight')
      ->setDisplayOptions('form', [
        'type' => 'physical_measurement_default',
        'settings' => [
          'default_unit' => 'kg',
          'allow_unit_change' => FALSE,
        ],
        'weight' => 9,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'physical_measurement_default',
        'weight' => 9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['price'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('Price'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'commerce_price_default',
        'weight' => 10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'commerce_price_default',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['inner_height'] = BaseFieldDefinition::create('physical_measurement')
      ->setLabel(t('Inside height'))
      ->setSetting('measurement_type', 'length')
      ->setDisplayOptions('form', [
        'type' => 'physical_measurement_default',
        'settings' => [
          'default_unit' => 'mm',
          'allow_unit_change' => FALSE,
        ],
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['sort_weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Sort weight'))
      ->setTranslatable(FALSE)
      ->setSetting('unsigned', FALSE)
      ->setDefaultValue(0)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 14,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status']
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 90,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(t('URL alias'))
      ->setDescription(t('The URL alias.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'path',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setComputed(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the trailer was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the trailer was last edited.'))
      ->setTranslatable(TRUE);

    return $fields;
  }

    /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return [\Drupal::currentUser()->id()];
  }

}
