<?php

namespace Drupal\trailer\Entity;

use Drupal\commerce_price\Price;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\physical\Length;
use Drupal\physical\Weight;
use Drupal\taxonomy\TermInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for trailers.
 */
interface TrailerInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface, EntityDescriptionInterface {

  /**
   * Gets the title.
   *
   * @return string
   *   The title.
   */
  public function getTitle();

  /**
   * Sets the title.
   *
   * @param string $title
   *   The title.
   *
   * @return $this
   */
  public function setTitle($title);

  /**
   * Gets the trailer creation timestamp.
   *
   * @return int
   *   The trailer creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Sets the trailer creation timestamp.
   *
   * @param int $timestamp
   *   The trailer creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the category.
   *
   * @return \Drupal\taxonomy\TermInterface|null
   *   The category term, or null.
   */
  public function getCategory();

  /**
   * Sets the category.
   *
   * @param \Drupal\taxonomy\TermInterface $category
   *   The category term.
   *
   * @return $this
   */
  public function setCategory(TermInterface $category);

  /**
   * Gets the category ID.
   *
   * @return int
   *   The category ID.
   */
  public function getCategoryId();

  /**
   * Sets the category ID.
   *
   * @param int $category_id
   *   The category ID.
   *
   * @return $this
   */
  public function setCategoryId($category_id);

  /**
   * Gets the tyres.
   *
   * @return string
   *   The tyres.
   */
  public function getTyres();

  /**
   * Sets the tyres.
   *
   * @param string $tyres
   *   The tyres.
   *
   * @return $this
   */
  public function setTyres($tyres);

  /**
   * Gets the loading height.
   *
   * @return \Drupal\physical\Length|null
   *   The loading height, or NULL if unknown.
   */
  public function getLoadingHeight();

  /**
   * Sets the loading height.
   *
   * @param \Drupal\physical\Length $loading_height
   *   The loading height.
   *
   * @return $this
   */
  public function setLoadingHeight(Length $loading_height);

  /**
   * Gets the internal length.
   *
   * @return \Drupal\physical\Length|null
   *   The internal length, or NULL if unknown.
   */
  public function getInternalLength();

  /**
   * Gets the internal width.
   *
   * @return \Drupal\physical\Length|null
   *   The internal width, or NULL if unknown.
   */
  public function getInternalWidth();

  /**
   * Gets the internal height.
   *
   * @return \Drupal\physical\Length|null
   *   The internal height, or NULL if unknown.
   */
  public function getInternalHeight();

  /**
   * Gets the load capacity.
   *
   * @return \Drupal\physical\Weight|null
   *   The load capacity, or NULL if unknown.
   */
  public function getLoadCapacity();

  /**
   * Sets the load capacity.
   *
   * @param \Drupal\physical\Weight $load_capacity
   *   The load capacity.
   *
   * @return $this
   */
  public function setLoadCapacity(Weight $load_capacity);

  /**
   * Gets the total weight.
   *
   * @return \Drupal\physical\Weight|null
   *   The total weight, or NULL if unknown.
   */
  public function getTotalWeight();

  /**
   * Sets the total weight.
   *
   * @param \Drupal\physical\Weight $total_weight
   *   The total weight.
   *
   * @return $this
   */
  public function setTotalWeight(Weight $total_weight);

  /**
   * Gets the price.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The price, or NULL.
   */
  public function getPrice();

  /**
   * Sets the price.
   *
   * @param \Drupal\commerce_price\Price $price
   *   The price.
   *
   * @return $this
   */
  public function setPrice(Price $price);

  /**
   * Gets the sort weight.
   *
   * @return int
   *   The sort weight.
   */
  public function getSortWeight();

  /**
   * Sets the sort weight.
   *
   * @param int $sort_weight
   *   The sort weight.
   *
   * @return $this
   */
  public function setSortWeight($sort_weight);

}
