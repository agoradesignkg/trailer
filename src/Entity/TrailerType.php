<?php

namespace Drupal\trailer\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the trailer type entity class.
 *
 * @ConfigEntityType(
 *   id = "trailer_type",
 *   label = @Translation("Trailer type"),
 *   label_singular = @Translation("Trailer type"),
 *   label_plural = @Translation("Trailer types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count trailer type",
 *     plural = "@count trailer types",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\trailer\Access\TrailerTypeAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\trailer\Form\TrailerTypeForm",
 *       "edit" = "Drupal\trailer\Form\TrailerTypeForm",
 *       "delete" = "Drupal\trailer\Form\TrailerTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\trailer\TrailerTypeListBuilder",
 *   },
 *   config_prefix = "trailer_type",
 *   admin_permission = "administer trailer types",
 *   bundle_of = "trailer",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/trailer-types/add",
 *     "edit-form" = "/admin/structure/trailer-types/{trailer_type}/edit",
 *     "delete-form" = "/admin/structure/trailer-types/{trailer_type}/delete",
 *     "collection" = "/admin/structure/trailer-types"
 *   }
 * )
 */
class TrailerType extends ConfigEntityBundleBase implements TrailerTypeInterface {

  /**
   * The trailer type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The trailer type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The trailer type description.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }
}
