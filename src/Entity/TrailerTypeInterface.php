<?php

namespace Drupal\trailer\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for trailer types.
 */
interface TrailerTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

}
