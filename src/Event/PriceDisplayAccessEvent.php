<?php

namespace Drupal\trailer\Event;

use Drupal\trailer\Entity\TrailerInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the trailer display price access event.
 *
 * @see \Drupal\trailer\Event\TrailerEvents
 */
class PriceDisplayAccessEvent extends Event {

  /**
   * The trailer entity.
   *
   * @var \Drupal\trailer\Entity\TrailerInterface
   */
  protected $trailer;

  /**
   * Flag, whether or not to allow price field display.
   *
   * @var bool
   */
  protected $allowed;

  /**
   * The cache contexts.
   *
   * @var string[]
   */
  protected $cacheContexts;

  /**
   * Constructs a new PriceDisplayAccessEvent.
   *
   * @param \Drupal\trailer\Entity\TrailerInterface $trailer
   *   The trailer entity.
   * @param string[] $cache
   *   The build cache, used to initialize the event's cache settings, that can
   *   be extended by calling functions.
   */
  public function __construct(TrailerInterface $trailer, array $cache = []) {
    $this->trailer = $trailer;
    $this->allowed = FALSE;
    $this->cacheContexts = !empty($cache) && !empty($cache['contexts']) ? $cache['contexts'] : [];
  }

  /**
   * Allow price field display.
   */
  public function allow() {
    $this->allowed = TRUE;
  }

  /**
   * Disallow price field display.
   */
  public function disallow() {
    $this->allowed = FALSE;
  }

  /**
   * Return whether or not to allow price field display.
   *
   * @return bool
   *   TRUE, if price field should be displayed, FALSE otherwise.
   */
  public function isAllowed() {
    return $this->allowed;
  }

  /**
   * Adds a cache context.
   *
   * @param string $cache_context
   *   A single cache context.
   */
  public function addCacheContext($cache_context) {
    $this->cacheContexts[] = $cache_context;
  }

  /**
   * Returns the collected cache contexts.
   *
   * @return string[]
   *   The collected cache contexts (initially provided plus added ones).
   */
  public function getCacheContexts() {
    return $this->cacheContexts;
  }

  /**
   * Returns the trailer entity.
   *
   * @return \Drupal\trailer\Entity\TrailerInterface
   *   The trailer entity.
   */
  public function getTrailer() {
    return $this->trailer;
  }

}
