<?php

namespace Drupal\trailer\Event;

/**
 * Holds constants of all custom events regarding trailer entities.
 */
final class TrailerEvents {

  /**
   * Name of the event fired on checking price field display access.
   *
   * @Event
   *
   * @see \Drupal\trailer\Event\PriceDisplayAccessEvent
   */
  const PRICE_DISPLAY_ACCESS = 'trailer.price.display_access';

}
