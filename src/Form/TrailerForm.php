<?php

namespace Drupal\trailer\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the trailer add/edit form.
 */
class TrailerForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\trailer\Entity\TrailerInterface $trailer */
    $trailer = $this->getEntity();
    $trailer->save();
    $this->messenger()->addStatus($this->t('The trailer %label has been successfully saved.', [
      '%label' => $trailer->label(),
    ]));
    $form_state->setRedirect('entity.trailer.canonical', ['trailer' => $trailer->id()]);
  }

}
