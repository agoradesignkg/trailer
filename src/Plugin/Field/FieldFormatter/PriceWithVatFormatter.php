<?php

namespace Drupal\trailer\Plugin\Field\FieldFormatter;

use Drupal\commerce_price\Plugin\Field\FieldFormatter\PriceDefaultFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Plugin implementation of the 'trailer_price_with_vat' formatter.
 *
 * @FieldFormatter(
 *   id = "trailer_price_incl_vat",
 *   label = @Translation("Price with VAT"),
 *   field_types = {
 *     "commerce_price"
 *   }
 * )
 */
class PriceWithVatFormatter extends PriceDefaultFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'vat_rate' => 19,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['vat_rate'] = [
      '#type' => 'number',
      '#min' => 0,
      '#max' => 100,
      '#step' => 1,
      '#title' => $this->t('VAT rate (in percent)'),
      '#default_value' => $this->getSetting('vat_rate'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $summary[] = $this->t('VAT rate: @vat_rate%.', ['@vat_rate' => $this->getSetting('vat_rate')]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $vat_rate = $this->getSetting('vat_rate');
    if (empty($vat_rate)) {
      return parent::viewElements($items, $langcode);
    }
    $vat_multiplicator = 100 + (int)$vat_rate;
    $vat_multiplicator = (string)$vat_multiplicator;
    /** @var \Drupal\commerce_price\RounderInterface $rounder_service */
    $rounder_service = \Drupal::service('commerce_price.rounder');

    $options = $this->getFormattingOptions();
    $elements = [];
    /** @var \Drupal\commerce_price\Plugin\Field\FieldType\PriceItem $item */
    foreach ($items as $delta => $item) {
      $price = $item->toPrice();
      $net_price_formatted = $this->currencyFormatter->format($item->number, $item->currency_code, $options);
      $gross_price = $price->multiply($vat_multiplicator)->divide('100');
      $gross_price = $rounder_service->round($gross_price);
      $gross_price_formatted = $this->currencyFormatter->format($gross_price->getNumber(), $gross_price->getCurrencyCode(), $options);
      $incl_vat_str = $this->t('incl. VAT');
      $elements[$delta] = [
        '#markup' => sprintf('%s (%s %s)', $net_price_formatted, $gross_price_formatted, $incl_vat_str),
        '#cache' => [
          'contexts' => [
            'languages:' . LanguageInterface::TYPE_INTERFACE,
            'country',
          ],
        ],
      ];
    }

    return $elements;
  }

}
