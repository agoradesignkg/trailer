<?php

namespace Drupal\trailer;

use Drupal\physical\NumberFormatterInterface;
use Drupal\trailer\Entity\TrailerInterface;

/**
 * Default trailer formatter implementation.
 */
class TrailerFormatter implements TrailerFormatterInterface {

  /**
   * The number formatter.
   *
   * @var \Drupal\physical\NumberFormatterInterface
   */
  protected $numberFormatter;

  /**
   * Constructs a new TrailerFormatter object.
   *
   * @param \Drupal\physical\NumberFormatterInterface $number_formatter
   *   The number formatter.
   */
  public function __construct(NumberFormatterInterface $number_formatter) {
    $this->numberFormatter = $number_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public function getInternalDimensionsString(TrailerInterface $trailer) {
    if ($trailer->get('internal_dimensions')->isEmpty()) {
      return '';
    }
    $dimensions_raw = [
      $trailer->getInternalLength(),
      $trailer->getInternalWidth(),
      $trailer->getInternalHeight(),
    ];
    $dimensions_raw = array_filter($dimensions_raw);
    $dimensions = [];
    $unit = '';
    /** @var \Drupal\physical\Length $dim */
    foreach ($dimensions_raw as $dim) {
      $dimensions[] = $this->numberFormatter->format($dim->round()->getNumber());
      if (empty($unit)) {
        $unit = $dim->getUnit();
      }
    }
    return implode(' x ', $dimensions) . ' ' . $unit;
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalWeightString(TrailerInterface $trailer) {
    if ($trailer->get('total_weight')->isEmpty()) {
      return '';
    }
    return $this->numberFormatter->format($trailer->getTotalWeight()->round()->getNumber()) . ' ' . $trailer->getTotalWeight()->getUnit();
  }

  /**
   * {@inheritdoc}
   */
  public function getLoadCapacityString(TrailerInterface $trailer) {
    if ($trailer->get('load_capacity')->isEmpty()) {
      return '';
    }
    return $this->numberFormatter->format($trailer->getLoadCapacity()->round()->getNumber()) . ' ' . $trailer->getLoadCapacity()->getUnit();
  }

  /**
   * {@inheritdoc}
   */
  public function getLoadingHeightString(TrailerInterface $trailer) {
    if ($trailer->get('loading_height')->isEmpty()) {
      return '';
    }
    return $this->numberFormatter->format($trailer->getLoadingHeight()->round()->getNumber()) . ' ' . $trailer->getLoadingHeight()->getUnit();
  }

}
