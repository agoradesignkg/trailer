<?php

namespace Drupal\trailer;

use Drupal\trailer\Entity\TrailerInterface;

/**
 * Defines the trailer formatter interface.
 */
interface TrailerFormatterInterface {

  /**
   * Formats the internal dimensions of the given trailer as string.
   *
   * @param \Drupal\trailer\Entity\TrailerInterface $trailer
   *   The trailer entity.
   *
   * @return string
   *   The internal dimensions formatted as string.
   */
  public function getInternalDimensionsString(TrailerInterface $trailer);

  /**
   * Formats the total weight of the given trailer as string.
   *
   * @param \Drupal\trailer\Entity\TrailerInterface $trailer
   *   The trailer entity.
   *
   * @return string
   *   The total weight formatted as string.
   */
  public function getTotalWeightString(TrailerInterface $trailer);

  /**
   * Formats the load capacity of the given trailer as string.
   *
   * @param \Drupal\trailer\Entity\TrailerInterface $trailer
   *   The trailer entity.
   *
   * @return string
   *   The load capacity formatted as string.
   */
  public function getLoadCapacityString(TrailerInterface $trailer);

  /**
   * Formats the loading height of the given trailer as string.
   *
   * @param \Drupal\trailer\Entity\TrailerInterface $trailer
   *   The trailer entity.
   *
   * @return string
   *   The loading height formatted as string.
   */
  public function getLoadingHeightString(TrailerInterface $trailer);

}
