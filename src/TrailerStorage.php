<?php

namespace Drupal\trailer;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The storage for trailer entities.
 */
class TrailerStorage extends SqlContentEntityStorage implements TrailerStorageInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a TrailerStorage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend to be used.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memory_cache
   *   The memory cache backend to be used.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(EntityTypeInterface $entity_type, Connection $database, EntityFieldManagerInterface $entity_field_manager, CacheBackendInterface $cache, LanguageManagerInterface $language_manager, MemoryCacheInterface $memory_cache, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user) {
    parent::__construct($entity_type, $database, $entity_field_manager, $cache, $language_manager, $memory_cache, $entity_type_bundle_info, $entity_type_manager);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('database'),
      $container->get('entity_field.manager'),
      $container->get('cache.entity'),
      $container->get('language_manager'),
      $container->get('entity.memory_cache'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * @inheritDoc
   */
  public function getActiveTrailerIds(array $exclude_ids = [], $bundle = 'default') {
    $query = $this->getQuery();
    $query->accessCheck(FALSE);
    $query->condition('status', 1);
    if (!empty($bundle)) {
      $query->condition('type', $bundle);
    }
    if (!empty($exclude_ids)) {
      $query->condition('trailer_id', $exclude_ids, 'NOT IN');
    }
    return $query->execute();
  }

  /**
   * @inheritDoc
   */
  public function getInactiveTrailerIds($bundle = 'default') {
    $query = $this->getQuery();
    $query->accessCheck(FALSE);
    $query->condition('status', 0);
    if (!empty($bundle)) {
      $query->condition('type', $bundle);
    }
    return $query->execute();
  }

  /**
   * @inheritDoc
   */
  public function getUsedCategoryIds() {
    $query = $this->database->select('trailer_field_data', 'tfd');
    $query->fields('tfd', ['category']);
    $query->distinct();
    $query->condition('tfd.status', 1);
    $query->condition('tfd.default_langcode', 1);
    $query->isNotNull('tfd.category');
    return $query->execute()->fetchCol();
  }

  /**
   * @inheritDoc
   */
  public function loadTrailersWithoutPrice($bundles = []) {
    $query = $this->database->select('trailer_field_data', 'tfd');
    $query->fields('tfd', ['trailer_id']);
    $query->condition('tfd.status', 1);
    $query->condition('tfd.default_langcode', 1);
    if (!empty($bundles)) {
      $query->condition('tfd.type', $bundles, 'IN');
    }
    $or_condition = $query->orConditionGroup();
    $or_condition->isNull('tfd.price__number');
    $or_condition->condition('tfd.price__number', '0.000000');
    $query->condition($or_condition);
    $result = $query->execute()->fetchAllAssoc('trailer_id');
    $trailers = [];
    if (!empty($result)) {
      $trailers = $this->loadMultiple(array_keys($result));
    }
    return $trailers;
  }

  /**
   * @inheritDoc
   */
  public function loadTrailersWithoutCategory($bundles = []) {
    $query = $this->database->select('trailer_field_data', 'tfd');
    $query->fields('tfd', ['trailer_id']);
    $query->condition('tfd.status', 1);
    $query->condition('tfd.default_langcode', 1);
    $query->isNull('tfd.category');
    if (!empty($bundles)) {
      $query->condition('tfd.type', $bundles, 'IN');
    }
    $result = $query->execute()->fetchAllAssoc('trailer_id');
    $trailers = [];
    if (!empty($result)) {
      $trailers = $this->loadMultiple(array_keys($result));
    }
    return $trailers;
  }

  /**
   * @inheritDoc
   */
  public function loadTrailerByTitle($title, $bundle = 'default') {
    if (empty($title)) {
      return NULL;
    }
    $query = $this->getQuery();
    $query->accessCheck(FALSE);
    $query->condition('title', $title);
    $query->condition('type', $bundle);
    $query->sort('status', 'DESC');
    $query->range(0, 1);
    $query_result = $query->execute();
    $trailer_id = $query_result ? reset($query_result) : NULL;
    return $trailer_id ? $this->load($trailer_id) : NULL;
  }

}
