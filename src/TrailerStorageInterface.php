<?php

namespace Drupal\trailer;

use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;

interface TrailerStorageInterface extends SqlEntityStorageInterface {

  /**
   * Gets active trailer IDs, optionally filtered by bundle and excluded IDs.
   *
   * This is useful for post import processing to deactivate all trailers that
   * are no longer existing in the import database.
   *
   * @param int[] $exclude_ids
   *   An array of trailer IDs to exclude form the result.
   * @param string $bundle
   *   The bundle to filter. Set to NULL or an empty string to skip bundle
   *   filtering.
   *
   * @return int[]
   *   An array of trailer entity IDs.
   */
  public function getActiveTrailerIds(array $exclude_ids = [], $bundle = 'default');

  /**
   * Gets inactive trailer IDs, optionally filtered by bundle.
   *
   * @param string $bundle
   *   The bundle to filter. Set to NULL or an empty string to skip bundle
   *   filtering.
   *
   * @return int[]
   *   An array of trailer entity IDs.
   */
  public function getInactiveTrailerIds($bundle = 'default');

  /**
   * Loads all available and currently used category IDs.
   *
   * @return \Drupal\taxonomy\TermInterface[]
   *   An array of currently used category (taxonomy term) IDs.
   */
  public function getUsedCategoryIds();

  /**
   * Loads all active trailers without a price.
   *
   * @param string[] $bundles
   *   Allows to restrict to the given bundles. Leave empty for no restriction.
   *
   * @return \Drupal\trailer\Entity\TrailerInterface[]
   *   A list of active trailer entities without price.
   */
  public function loadTrailersWithoutPrice($bundles = []);

  /**
   * Loads all active trailers without a category.
   *
   * @param string[] $bundles
   *   Allows to restrict to the given bundles. Leave empty for no restriction.
   *
   * @return \Drupal\trailer\Entity\TrailerInterface[]
   *   A list of active trailer entities without category.
   */
  public function loadTrailersWithoutCategory($bundles = []);

  /**
   * Loads a single trailer by title (which is in fact a SKU at Unsinn).
   *
   * Note, that always a single result will be returned (or NULL of course),
   * even if the title is ambiguous (then the first row is returned).
   *
   * Please also note, that the query filters not by inactive trailers at all.
   * But at least the status is used as sort criteria. So in case of having
   * ambiguous titles with different publishing states, the published ones will
   * have a higher priority.
   *
   * @param string $title
   *   The trailer title.
   * @param string $bundle
   *   The bundle to restrict to. Defaults to 'default'.
   *
   * @return \Drupal\trailer\Entity\TrailerInterface|null
   *   The loaded trailer entity, or NULL, if no match was found.
   */
  public function loadTrailerByTitle($title, $bundle = 'default');

}
