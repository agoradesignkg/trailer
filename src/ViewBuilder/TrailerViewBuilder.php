<?php

namespace Drupal\trailer\ViewBuilder;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\trailer\Event\PriceDisplayAccessEvent;
use Drupal\trailer\Event\TrailerEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Defines the entity view builder for trailer entities.
 */
class TrailerViewBuilder extends EntityViewBuilder {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->eventDispatcher = $container->get('event_dispatcher');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterBuild(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
    /** @var \Drupal\trailer\Entity\TrailerInterface $entity */
    if (!empty($build['price'])) {
      $display_price_event = new PriceDisplayAccessEvent($entity, $build['#cache']);
      $this->eventDispatcher->dispatch($display_price_event, TrailerEvents::PRICE_DISPLAY_ACCESS);
      $build['price']['#access'] = $display_price_event->isAllowed();
      $build['#cache']['contexts'] = $display_price_event->getCacheContexts();
    }
  }

}
